import Tile from './modules/Tile';
import Exit from './modules/Exit';
// import Person from './modules/Person';
import './index.scss';

const GRID_SIZE = 100;

const App = {
  currentTile: null,
  dragging: false,
  tiles: [],
  exits: [],
  people: [],
  tilesElement: null,
  peopleElement: null,
};

window.App = App;

const opposites = {
  north: 'south',
  south: 'north',
  east: 'west',
  west: 'east',
};

const reflect = ordinal => opposites[ordinal];

const connect = () => {
  if (App.tiles.length < 1) return;

  const currentObject = App.currentTile || App.currentPerson || App.currentExit;

  const { top, left } = currentObject.element.style;
  const x = parseInt(left, 10);
  const y = parseInt(top, 10);
  const neighbors = [
    { ordinal: 'north', x, y: y - GRID_SIZE },
    { ordinal: 'east', x: x + GRID_SIZE, y },
    { ordinal: 'south', x, y: y + GRID_SIZE },
    { ordinal: 'west', x: x - GRID_SIZE, y },
  ];
  App.tiles.forEach((tile) => {
    neighbors.forEach((neighbor) => {
      const { x, y } = neighbor;
      if (x === parseInt(tile.element.style.left, 10) && y === parseInt(tile.element.style.top, 10)) {
        currentObject.neighbors[neighbor.ordinal] = tile;
        tile.neighbors[reflect(neighbor.ordinal)] = currentObject;
      }
    });
  });
};

const placeTile = () => {
  const currentObject = App.currentTile || App.currentPerson || App.currentExit;
  const { top, left } = currentObject.element.style;
  const x = parseInt(left, 10);
  const y = parseInt(top, 10);
  App.tiles.forEach((tile) => {
    console.log(parseInt(tile.element.style.top, 10), parseInt(tile.element.style.left, 10));
  });

  // TODO: prevent placing on top of existing tile
  connect();

  App.tiles.push(App.currentTile);
  App.dragging = false;
  App.currentTile.listen();
  App.currentTile = null;

  document.body.removeEventListener('click', placeTile);
};

const placeExit = () => {
  connect();

  // App.exits.push(App.currentExit);
  App.tiles.push(App.currentExit);
  App.dragging = false;
  App.currentExit = null;

  document.body.removeEventListener('click', placeExit);
};

const addTile = () => {
  const tile = new Tile(App.tiles.length);
  App.currentTile = tile;
  App.tilesElement.appendChild(tile.element);
  App.dragging = true;

  setTimeout(() => { document.body.addEventListener('click', placeTile); }, 10);
};

const addExit = () => {
  const exit = new Exit(App.exits.length);
  App.currentExit = exit;
  App.tilesElement.appendChild(exit.element);
  App.dragging = true;

  setTimeout(() => { document.body.addEventListener('click', placeExit); }, 10);
};

const mousemove = ({ clientX, clientY }) => {
  const currentObject = App.currentTile || App.currentPerson || App.currentExit;
  if (currentObject && App.dragging) {
    const x = (Math.round(clientX / GRID_SIZE) * GRID_SIZE) - GRID_SIZE / 2;
    const y = (Math.round(clientY / GRID_SIZE) * GRID_SIZE) - GRID_SIZE / 2;
    currentObject.element.style.top = `${y}px`;
    currentObject.element.style.left = `${x}px`;
  }
};


const init = () => {
  App.element = document.createElement('div');
  App.element.className = 'app';

  const addTileButton = document.createElement('button');
  const addExitButton = document.createElement('button');
  addTileButton.innerText = 'Add Tile';
  addExitButton.innerText = 'Add Exit';
  addTileButton.setAttribute('data-type', 'tile');
  addExitButton.setAttribute('data-type', 'exit');
  addTileButton.addEventListener('click', addTile);
  addExitButton.addEventListener('click', addExit);


  const buttons = document.createElement('div');
  buttons.className = 'buttons';

  App.tilesElement = document.createElement('div');
  App.peopleElement = document.createElement('div');

  App.tilesElement.className = 'tiles';
  App.peopleElement.className = 'people';

  buttons.appendChild(addTileButton);
  buttons.appendChild(addExitButton);
  App.element.appendChild(App.peopleElement);
  App.element.appendChild(buttons);
  App.element.appendChild(App.tilesElement);
  document.body.appendChild(App.element);


  document.body.addEventListener('mousemove', mousemove);
};

init();
