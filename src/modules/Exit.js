export default class Exit {
  constructor(id) {
    this.id = id;
    this.element = document.createElement('div');
    this.element.className = 'exit';

    this.neighbors = {
      north: null,
      south: null,
      east: null,
      west: null,
    };
  }
}
