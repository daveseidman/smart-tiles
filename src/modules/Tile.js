const angles = {
  north: '0deg',
  east: '90deg',
  south: '180deg',
  west: '270deg',
};

const updateSpeed = 100;

export default class Tile {
  constructor(id) {
    this.update = this.update.bind(this);
    this.push = this.push.bind(this);
    this.neighborsStateChanged = this.neighborsStateChanged.bind(this);

    this.distanceToExit = Number.POSITIVE_INFINITY;

    this.pressed = false;

    this.neighbors = {
      north: null,
      south: null,
      east: null,
      west: null,
    };

    this.neighbors = { north: null, south: null, east: null, west: null };
    this.prevNeighbors = { north: null, south: null, east: null, west: null };
    this.element = document.createElement('div');
    this.arrow = document.createElement('img');
    this.arrow.src = 'assets/images/arrow.svg';
    this.element.className = `tile ${id === undefined ? 'exit' : ''}`;
    this.distance = document.createElement('div');
    this.arrow.className = 'tile-arrow hidden';
    this.distance.className = 'tile-distance';
    this.element.appendChild(this.arrow);
    this.element.appendChild(this.distance);

    this.update();
  }

  // when the tile's button is pressed (at least one person steps on it)
  press() {

  }

  // when the tile's button is released (everyone has stepped off this tile)
  release() {

  }

  update() {
    let distance = Number.POSITIVE_INFINITY;
    let exitDirection = null;

    const keys = Object.keys(this.neighbors);
    for (let i = 0; i < keys.length; i += 1) {
      const neighbor = this.neighbors[keys[i]];
      if (neighbor) {
        if (neighbor.element.className === 'exit') {
          distance = 0;
          exitDirection = keys[i];
          break;
        }
        if (neighbor.distanceToExit < distance) {
          distance = neighbor.distanceToExit + 1;
          exitDirection = keys[i];
        }
      }
    }

    this.distanceToExit = distance;
    this.distance.innerText = this.distanceToExit === Number.POSITIVE_INFINITY ? '' : this.distanceToExit;
    this.arrow.classList[this.distanceToExit < Number.POSITIVE_INFINITY ? 'remove' : 'add']('hidden');
    this.arrow.style.transform = `rotate(${angles[exitDirection]})`;
    setTimeout(this.update, updateSpeed);
  }
}
