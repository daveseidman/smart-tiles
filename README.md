# Smart Tiles

Physical device design.  
Tiles should operate independently and be "snappable" in any space.  

They should have a single input at first, which would indicate whether a tile is being stepped on or not. Future versions could measure how much weight is on it.

Tiles have 4 sides.  Tiles can communicate with it's neighbors to say whether or not there is a person on it and whether or not it has a side that borders an exit.

Tiles don't know their overall position.  


## TODO

- Add router  
- Show arrows on tiles
